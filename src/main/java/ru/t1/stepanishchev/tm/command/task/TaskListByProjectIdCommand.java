package ru.t1.stepanishchev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.model.Task;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-show-by-project-id";

    @NotNull
    private final String DESCRIPTION = "Show task list by project id.";

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final List<Task> tasks = getTaskService().findAllByProjectId(userId, projectId);
        renderTasks(tasks);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}