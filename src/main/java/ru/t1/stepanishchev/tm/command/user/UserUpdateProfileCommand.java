package ru.t1.stepanishchev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "update-user-profile";

    @NotNull
    private final String DESCRIPTION = "Update profile of current user.";

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

}